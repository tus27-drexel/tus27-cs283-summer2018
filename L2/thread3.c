#include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define THREADSnum 10000


int foo =  0; //shared variable
pthread_mutex_t mutex_foo;

void *thread(void *arg){
	int i;
	pthread_mutex_lock(&mutex_foo);
	for(i=0; i<100; i++){
		//locking inside for loop
			foo++;
		}
	pthread_mutex_unlock(&mutex_foo);
	pthread_exit(NULL);
}

int main(int argc, char *argv[]){
	void *stat;
	pthread_mutex_init(&mutex_foo, NULL);
	pthread_t threads[THREADSnum];
	int j;
	int bar;
	clock_t begin = clock();
	for(j=0; j<THREADSnum; j++){
		bar = pthread_create(&threads[j], NULL, thread, (void *)NULL);
	}
	for (j =0; j<THREADSnum; j++){
		pthread_join(threads[j], &stat);
	}
	clock_t end = clock();
	printf("foo = %d\n", foo);
	double time_spent = (double)(end - begin)/CLOCKS_PER_SEC;
	printf("elapsed time = %f\n",	time_spent);
	pthread_exit(NULL);
}

