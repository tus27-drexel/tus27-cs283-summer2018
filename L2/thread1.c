#include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define THREADSnum 10000


int foo =  0; //shared variable

void *thread(void *arg){
	int i;
	for(i=0; i<100; i++){
		foo++;
	}
	pthread_exit(NULL);
}

int main(int argc, char *argv[]){
	pthread_t threads[THREADSnum];
	int j;
	int bar;
	clock_t begin = clock();
	for(j=0; j<THREADSnum; j++){
		bar = pthread_create(&threads[j], NULL, thread, (void *)NULL);
		if(bar){
			printf("return from pthread_create() is %d\n", bar);
			exit(-1);
		}
	}
	clock_t end = clock();
	printf("foo = %d\n", foo);
	double time_spent = (double)(end - begin)/CLOCKS_PER_SEC;
	printf("elapsed time = %f\n", time_spent);
	pthread_exit(NULL);
}

