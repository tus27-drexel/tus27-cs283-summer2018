#include<stdio.h>
#include<stdlib.h>
#include<time.h>
/*Finally, write a program that, using malloc and realloc, creates an array of
initial size n.  Write add(), remove() and get() functions for your array.
When adding beyond the end of the array, reallocate space such that the
array contains one more element.  Time your program for adding 100000
elements (or more).  Then modify the program such that it increases in size
by a factor of 2 times the previous size.  Time it again.  What do you
observe?*/


void add(int val);
void delete(int i); //i=index
int get(int i);

int* arr;
int arrSize;
int numElements;
int factor = 2;
int num = 100000;


int main(int argc, char** argv){
	clock_t begin = clock();
	arrSize = 10;
	arr =  malloc(arrSize*sizeof(int));

	for(int i=0; i<num; i++){
		add(rand()%100);
	}	
	clock_t end = clock();
	//double timeSpent = (end - begin)/CLOCKS_PER_SEC;
	printf("Elapsed: %f seconds\n", (double)(end-begin)/CLOCKS_PER_SEC);
	return 0;
}



void add(int x){
	if(numElements >= arrSize){
		int size  = factor*arrSize;
		arr = realloc(arr, size*sizeof(int));
		arrSize = size;
	}
	arr[numElements] = x;
	numElements++;
}

void delete(int i){
	int j;

	for(j = i; j<numElements-1; j++){
		arr[j] = arr[j+1];
	}
	numElements--;
}

int get(int i){
	return arr[i];
}


