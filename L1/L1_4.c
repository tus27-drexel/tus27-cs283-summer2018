#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>

struct Node{
	int val;
	struct Node* next;
	struct Node* prev;
};

void sort(struct Node * head, int Lsize); //Lsize = size of list
void BubSort(struct Node * head, int Lsize, int lastVal);

int main(int argc, char** argv){
	int Lsize = 10;
	struct Node*head = malloc(sizeof(struct Node));
	head -> prev = NULL;
	struct Node*curr;
	int i = 1;//counter
	curr = head;
	do{
	
		curr -> val = rand()%100;
		if(i < Lsize){
			curr -> next =  malloc(sizeof(struct Node));
			curr -> next -> prev = curr;
			i++;
		} 
		else{		
		curr -> next = NULL;
		}
	}while((curr->next != NULL) && (curr = curr->next));
	
	
	
	//This part is where I cannot print the unsorted list

	printf("Unsorted list:\n ");
	while(curr != NULL){
		printf("%d\n", curr -> val);
		curr = curr -> next;
	}



	sort(head, Lsize);
	curr = head;
	printf("Sorted List: \n");
	while(curr != NULL){
		printf("%d\n", curr -> val);
		curr = curr -> next;
	}
	return 0;
}
void sort(struct Node* head, int size)
{
	BubSort(head, size, size-1);

}

void BubSort(struct Node * head, int Lsize, int lastVal){
	if (lastVal < 0){
		return;
	}
	
	struct Node * curr = head;
	struct Node * next =  head->next;
	int ind0 = 0;
	int ind1 = 1;

	while((ind0 <= lastVal) && (ind1<Lsize)){
		if((next->val) < (curr->val)){
			int x = curr -> val;
			int y = next -> val;
			next -> val = x;
			curr -> val = y;
		}

		ind0++;
		ind1++;
		curr = next;
		next = curr->next;
	}
	lastVal--;
	BubSort(head, Lsize, lastVal);

}

