#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>

void sort(int *a, int size);
void BubSort(int *a, int size, int lastVal);

int main(){
	int arrSize = 20;
	int * arr = malloc(arrSize * sizeof(int));
   int i=0;
	int* b;
	for(i=0;i<arrSize;i++){//populating array
		b = arr+i;
		*b = rand()%100;
	}
	int j=0; //loop for original vals
	for(j=0; j<arrSize; j++){
		b = arr+j;
		printf("Unsorted array[%d]=%d\n", j, *b);
	}
	sort(arr, arrSize);
	int k=0; //sorted array
	for(k=0; k<arrSize; k++){
		b = arr+k;
		printf("Sorted Array: [%d] = %d\n", k, *b);
	}
	return 0;
}


void sort(int *a, int size){
	 BubSort(a, size, size-1);
}

void BubSort(int *a, int size, int lastVal){
	if(lastVal < 0){
		return;
	}
	int ind0 = 0;
	int ind1 = 1;
	while((ind1 <= lastVal) && (ind1 < size)){
		if(a[ind1]<a[ind0]){
			int x = a[ind0];
			int y = a[ind1];
			a[ind1] = x;
			a[ind0] = y;
		}
		ind0++;
		ind1++;
	}
	lastVal--;
	BubSort(a, size, lastVal);
}

