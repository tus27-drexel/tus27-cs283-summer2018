
#include "csapp.h"
void echo(int connfd);

int main(int argc, char **argv){
	printf("main works \n");
	int listenfd, connfd, port, clientlen;
	struct sockaddr_in clientaddr;
   struct hostent *hp;
	char *haddrp;
	port = atoi(argv[1]); /* the server listens on a port passsed on the
									 command line */
	listenfd = open_listenfd(port);
	printf("%d\n", listenfd);
   while(1){
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
      hp = Gethostbyaddr((const char*)&clientaddr.sin_addr.s_addr, sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);
		echo(connfd);
      Close(connfd);
	}	
}

void echo(int connfd) { 	
	size_t n;  
	char buf[MAXLINE];  
	rio_t rio;
  int i = 0;	
	Rio_readinitb(&rio, connfd); 
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) { 
		//upper_case(buf);
		Rio_writen(connfd, buf, n); 
		printf("server received %lu bytes\n", n);
		for(i = 0; i<sizeof(buf); i++){
			printf("%s\n", buf[i]);
		}
  	} 
} 

