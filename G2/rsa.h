#ifndef _RSA_HEAD_
#define _RSA_HEAD_

struct key{
	long a,long b;
}

long nthprime(int n);
int isPrime(int n);
long coprime(long a); 
int GCD(int a, int b);
int mod_inverse(int base, int m);
int modulo(int a, int b, int c);
int totient(int x);
int endecrypt(int message, int key, int c);
