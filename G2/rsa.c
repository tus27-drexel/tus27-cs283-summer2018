#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include "rsa.h"

int nthprime(int n){
	if(n <= 0){
		return 0;
	}

	if(n == 1){
		return 2;
	}
	else{	
		long count = 0;
		long i = 2;
		long rv = 0;
		while(count < n){
			if( isPrime(i)){
				count++;
			}
			i++;
		}
		return i-1;
	}	
}

long coprime(long a){
//if two numbers GCD=1 then coprime
//two integers are coprime when they have no prime factors between them
//Generates random prime numbers

int seed = (int)time(0);
srand (seed);
int i = rand()%1000;
long prime = nthprime(i+1);

while(a%prime == 0){
	i = (int)time(0);
	srand (seed);
	i = rand()%1000;
	prime = nthprime(i+1);
}
return prime;
}

int isPrime(long n){
	int x = n/2;
	int i;
	int isPrime = 1;

	for(i=2; i<x; i++){
			if(n%i == 0){
				isPrime = 0;
				break;
			}
		}
	return isPrime;
	}

